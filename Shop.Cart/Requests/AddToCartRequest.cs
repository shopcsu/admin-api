namespace Shop.Cart.Requests;

public class AddToCartRequest
{
    public int Id { get; set; }
    public int Quantity { get; set; }
}