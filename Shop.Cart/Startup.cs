using Shop.Cart.Client;
using Shop.Common.Extensions.Amqp;
using Shop.Common.Extensions.Exceptions;
using Shop.Common.Extensions.Json;
using Shop.Common.Extensions.Redis;
using Shop.Common.Extensions.Swagger;

namespace Shop.Cart;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSwagger()
            .AddRedis()
            .AddAmqp()
            .AddJsonConfig()
            .AddControllers()
            .AddJsonConfig();

        services.AddOptions<WebClientOptions>()
            .BindConfiguration("Api");
        services.AddHttpClient<AdminWebClient>();

        services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
        {
            builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
        }));
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseApiExceptionHandler();

        if (env.IsDevelopment()) app.UseSwaggerPage();

        app.UseRouting();
        app.UseCors("CorsPolicy");

        app.UseAuthentication();
        app.UseAuthorization();
        app.UseHttpLogging();

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
}