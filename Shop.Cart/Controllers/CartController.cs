using System.Net;
using System.Net.Mime;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using Shop.Cart.Client;
using Shop.Cart.Requests;
using Shop.Common.Extensions.Amqp;
using Shop.Common.Extensions.Common;
using Shop.Common.Extensions.Exceptions;
using Shop.Domain.Dtos;
using Shop.Domain.Requests.Orders;
using StackExchange.Redis;

namespace Shop.Cart.Controllers;

[ApiController]
[Route("cart")]
public class CartController : ControllerBase
{
    private readonly IConnectionMultiplexer _redis;
    private readonly AdminWebClient _client;

    public CartController(IConnectionMultiplexer redis, AdminWebClient client)
    {
        _redis = redis;
        _client = client;
    }

    [HttpGet]
    public async Task<CartDto> GetCart()
    {
        var db = _redis.GetDatabase();

        if (!int.TryParse(HttpContext.Request.Headers["Id"], out var id))
        {
            return null;
        }

        var cached = await db.StringGetAsync(id.ToString());
        return cached.HasValue ? JsonSerializer.Deserialize<CartDto>(cached) : null;
    }

    [HttpDelete("{id:int}")]
    public async Task DeleteFromCart([FromRoute] int id)
    {
        var db = _redis.GetDatabase();

        if (!int.TryParse(HttpContext.Request.Headers["Id"], out var uid))
        {
            throw new RestException("user id not found", HttpStatusCode.Forbidden);
        }

        var cached = await db.StringGetAsync(uid.ToString());
        var cart = cached.HasValue ? JsonSerializer.Deserialize<CartDto>(cached) : null;

        if (cart is null)
        {
            throw new RestException("cart not found", HttpStatusCode.NotFound);
        }

        var item = cart.Items.FirstOrDefault(x => x.Id == id);
        if (item != null)
        {
            cart.Items.Remove(item);
            cart.TotalPrice = cart.Items.Select(x => x.Price).Sum();
            db.StringSet(uid.ToString(), JsonSerializer.Serialize(cart), TimeSpan.FromDays(2));
        }
    }
    
    [HttpPost]
    public async Task AddToCart([FromBody] AddToCartRequest request)
    {
        var db = _redis.GetDatabase();

        if (!int.TryParse(HttpContext.Request.Headers["Id"], out var uid))
        {
            throw new RestException("user id not found", HttpStatusCode.Forbidden);
        }

        var cached = await db.StringGetAsync(uid.ToString());
        var cart = (cached.HasValue ? JsonSerializer.Deserialize<CartDto>(cached) : null) ?? new CartDto();

        if (cart.Items.Any(x => x.Id == request.Id))
        {
            cart.Items.First(x => x.Id == request.Id)
                .Also(x => x.Price = x.Price / x.Quantity * request.Quantity)
                .Also(x => x.Quantity = request.Quantity);
        }
        else
        {
            var item = await _client.GetItem(request.Id);
            Console.WriteLine(item.Id);
            if (item == null)
            {
                throw new RestException("item not found", HttpStatusCode.NotFound);
            }

            cart.Items.Add(
                item.Also(x => x.Quantity = request.Quantity)
                    .Also(x => x.Price *= request.Quantity)
            );
            Console.WriteLine(item.Title);
        }
        
        cart.TotalPrice = cart.Items.Select(x => x.Price).Sum();
        db.StringSet(uid.ToString(), JsonSerializer.Serialize(cart), TimeSpan.FromHours(120));
    }

    [HttpPost("buy")]
    public async Task MakeOrder([FromServices] IModel amqp, [FromServices] IOptions<AmqpOptions> options)
    {
        var db = _redis.GetDatabase();

        if (!int.TryParse(HttpContext.Request.Headers["Id"], out var id))
        {
            throw new RestException("Cart is empty", HttpStatusCode.BadRequest);
        }

        var cached = await db.StringGetAsync(id.ToString());
        var cart = cached.HasValue ? JsonSerializer.Deserialize<CartDto>(cached) : null;

        if (cart is null)
        {
            throw new RestException("Cart is empty", HttpStatusCode.BadRequest);
        }
        
        var props = amqp.CreateBasicProperties()
            .Also(x => x.ContentType = MediaTypeNames.Application.Json)
            .Also(x => x.Timestamp = new AmqpTimestamp(DateTime.UtcNow.ToUnixTimestamp()));

        var request = new MakeOrderRequest
        {
            Cart = cart,
            Created = DateTime.UtcNow,
            UserId = id
        };

        amqp.BasicPublish(options.Value.Exchange,
            request.GetType().Name,
            props,
            JsonSerializer.SerializeToUtf8Bytes(request)
        );

        await db.KeyDeleteAsync(id.ToString());
    }
}