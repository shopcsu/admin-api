using System.Net.Http.Headers;
using System.Text.Json;
using Mapster;
using Microsoft.Extensions.Options;
using Shop.Domain.Dtos;

namespace Shop.Cart.Client;

public class AdminWebClient
{
    private readonly HttpClient _client;

    public AdminWebClient(HttpClient client, IOptions<WebClientOptions> options)
    {
        client.BaseAddress = options.Value.Uri;
        _client = client;
    }

    public async Task<ItemDto> GetItem(int id)
    {
        var resp = await _client.GetAsync($"admin/items/{id}");
        var content = await resp.Content.ReadAsStringAsync();
        Console.WriteLine(content);
        var item = JsonSerializer.Deserialize<Resp>(content)!;
        Console.WriteLine(item.Id);
        return item.Adapt<ItemDto>();
    }
}

public class Resp
{
    public int Id { get; set; }
    public string Title { get; set; }
    public int Price { get; set; }
}