namespace Shop.Auth.Dtos;

public class TokenDto
{
    public string AuthToken { get; set; }
    public string RefreshToken { get; set; }
    public DateTime ExpiresAt { get; set; }
}