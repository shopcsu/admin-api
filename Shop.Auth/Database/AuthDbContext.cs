using Microsoft.EntityFrameworkCore;
using Shop.Domain.Models;

namespace Shop.Auth.Database;

public class AuthDbContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Token> Tokens { get; set; }
    
    public AuthDbContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>()
                    .Property(x => x.Role)
                    .HasConversion<string>();

        modelBuilder.Entity<User>()
                    .HasIndex(x => x.Name)
                    .IsUnique();
        
        base.OnModelCreating(modelBuilder);
    }
}