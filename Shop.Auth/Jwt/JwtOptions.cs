﻿namespace Shop.Auth.Jwt;

public class JwtOptions
{
    public string Issuer { get; set; }
    public string SigningKey { get; set; }
    public TimeSpan AccessTokenLifetime { get; set; }
    public TimeSpan RefreshTokenLifetime { get; set; }
}