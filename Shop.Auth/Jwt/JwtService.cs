﻿using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shop.Auth.Database;
using Shop.Auth.Dtos;
using Shop.Common.Extensions.Common;
using Shop.Common.Extensions.Exceptions;
using Shop.Domain.Interfaces;
using Shop.Domain.Models;

namespace Shop.Auth.Jwt;

public class JwtService
{
    private static readonly JwtSecurityTokenHandler JwtHandler = new();
    private readonly AuthDbContext _dbContext;
    private readonly TokenValidationParameters _parameters;
    private readonly JwtOptions _options;
    private readonly DateTime _now;

    public JwtService(AuthDbContext dbContext,
                      IOptions<JwtOptions> options,
                      IOptions<TokenValidationParameters> parameters)
    {
        _dbContext = dbContext;
        _options = options.Value;
        _parameters = parameters.Value;

        _now = DateTime.UtcNow;
    }

    #region GenerateToken

    public TokenDto GenerateToken(User user, ITokenInfo tokenInfo)
    {
        var token = user.Tokens
                        .FirstOrDefault(t => t.UserAgent == tokenInfo.UserAgent &&
                                             t.IpAddress.Equals(tokenInfo.IpAddress));

        if (token != null)
        {
            token.Value = Guid.NewGuid();
        }
        else
        {
            token = new Token
            {
                IpAddress = tokenInfo.IpAddress,
                UserAgent = tokenInfo.UserAgent
            };
            user.Tokens.Add(token);
        }

        return GenerateToken(user, token);
    }

    private TokenDto GenerateToken(User user, Token token)
    {
        if (user.Tokens.All(x => x.Id != token.Id))
        {
            user.Tokens.Add(token);
        }

        return new TokenDto
               {
                   AuthToken = GenerateAccessToken(user),
                   RefreshToken = GenerateRefreshToken(token)
               };
    }

    private static IEnumerable<Claim> ResolveUserClaims(User user)
    {
        yield return new Claim("Id", user.Id.ToString(), ClaimValueTypes.String);
        yield return new Claim("Role", user.Role.ToString(), ClaimValueTypes.String);
    }

    private static IEnumerable<Claim> ResolveTokenClaims(Token token)
    {
        yield return new Claim(JwtRegisteredClaimNames.Jti, token.Value.ToString(),
            ClaimValueTypes.String);
        yield return new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToUnixTimestamp().ToString(),
            ClaimValueTypes.Integer64);
    }

    private string GenerateAccessToken(User user)
    {
        var jwt = new JwtSecurityToken(
            claims: ResolveUserClaims(user),
            issuer: _options.Issuer,
            expires: _now + _options.AccessTokenLifetime,
            signingCredentials: GetSigningCredentials()
        );

        return JwtHandler.WriteToken(jwt);
    }

    private string GenerateRefreshToken(Token token)
    {
        var jwt = new JwtSecurityToken(
            claims: ResolveTokenClaims(token),
            issuer: _options.Issuer,
            notBefore: _now + _options.AccessTokenLifetime,
            expires: _now + _options.RefreshTokenLifetime,
            signingCredentials: GetSigningCredentials()
        );

        return JwtHandler.WriteToken(jwt);
    }

    private SigningCredentials GetSigningCredentials() =>
        new(_parameters.IssuerSigningKey, SecurityAlgorithms.HmacSha256);

    #endregion

    #region RefreshToken

    public async Task<TokenDto> RefreshTokenAsync(string token,
                                                  CancellationToken cancellationToken = default)
    {
        var tokenId = ParseRefreshToken(token);

        var user = await _dbContext.Users
                                   .Include(x => x.Tokens)
                                   .FirstOrDefaultAsync(x => x.Tokens
                                                              .Any(t => t.Value == tokenId), cancellationToken);

        if (user == null)
        {
            throw new RestException("Токен использован или отозван", HttpStatusCode.Forbidden);
        }

        var tokenModel = user.Tokens
                             .First(t => t.Value == tokenId);

        tokenModel.Value = Guid.NewGuid();
        return GenerateToken(user, tokenModel);
    }

    public Guid ParseRefreshToken(string token)
    {
        try
        {
            var principal = JwtHandler.ValidateToken(token, _parameters, out _);
            var tokenId = principal.FindFirst(JwtRegisteredClaimNames.Jti);

            if (tokenId == null)
            {
                throw new RestException("Не удалось получить идентификатор токена", HttpStatusCode.Unauthorized);
            }

            return Guid.Parse(tokenId.Value);
        }
        catch (Exception e)
        {
            throw new RestException(e.Message, "JwtException", HttpStatusCode.BadRequest, e);
        }
    }

    #endregion
}