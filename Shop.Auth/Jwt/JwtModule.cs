﻿using System.Net;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shop.Common.Extensions.Exceptions;

namespace Shop.Auth.Jwt;

public static class JwtModule
{
    public static IServiceCollection AddJwt(this IServiceCollection services)
    {
        services.AddOptions<JwtOptions>()
                .BindConfiguration("Jwt")
                .Validate(x => Encoding.UTF8.GetBytes(x.SigningKey ?? "").Length >= 16,
                     "Signing key must be greater than 128 bits");

        services.AddOptions<TokenValidationParameters>()
                .Configure<IOptions<JwtOptions>>((x, options) =>
                 {
                     x.ValidateAudience = false;
                     x.ValidateLifetime = true;
                     x.ValidateIssuerSigningKey = true;
                     x.ValidIssuer = options.Value.Issuer;
                     x.IssuerSigningKey = new SymmetricSecurityKey(
                         Encoding.UTF8.GetBytes(options.Value.SigningKey));
                     x.ClockSkew = TimeSpan.Zero;
                 });

        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer();

        services.AddScoped<JwtService>();
        services.ConfigureOptions<ConfigureJwtBearerOptions>();

        return services;
    }
}

public class ConfigureJwtBearerOptions : IPostConfigureOptions<JwtBearerOptions>
{
    private readonly TokenValidationParameters _parameters;

    public ConfigureJwtBearerOptions(IOptions<TokenValidationParameters> parameters) => 
        _parameters = parameters.Value;

    public void PostConfigure(string name, JwtBearerOptions options)
    {
        options.Events = new JwtBearerEvents
        {
            OnChallenge = ctx =>
            {
                if (ctx.Response.HasStarted || ctx.Handled || ctx.AuthenticateFailure == null)
                {
                    return Task.CompletedTask;
                }

                ctx.HandleResponse();

                throw new RestException("Unauthorized", HttpStatusCode.Unauthorized);
            }
        };

        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = _parameters;
    }
}