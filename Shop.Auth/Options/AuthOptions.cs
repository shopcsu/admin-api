namespace Shop.Auth.Options;

public class AuthOptions
{
    public string Salt { get; set; }
    public string Pepper { get; set; }
}