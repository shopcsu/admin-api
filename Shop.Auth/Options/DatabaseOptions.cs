namespace Shop.Auth.Options;

public class DatabaseOptions
{
    public string Connection { get; set; }
    public bool EnableLogs { get; set; }
}