using System.Net;
using Shop.Domain.Interfaces;

namespace Shop.Auth.Requests;

public class AuthRequest : ITokenInfo
{
    public string Name { get; set; }
    public string Password { get; set; }
    public IPAddress IpAddress { get; set; }
    public string UserAgent { get; set; }
}