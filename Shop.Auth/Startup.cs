using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Shop.Auth.Database;
using Shop.Auth.Jwt;
using Shop.Auth.Options;
using Shop.Common.Extensions.Exceptions;
using Shop.Common.Extensions.Json;
using Shop.Common.Extensions.Swagger;

namespace Shop.Auth;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSwagger()
                .AddJwt()
                .AddControllers()
                .AddJsonConfig();

        services.AddJsonConfig();
        
        services.AddOptions<DatabaseOptions>()
                .BindConfiguration("Database");
        
        services.AddOptions<AuthOptions>()
                .BindConfiguration("Auth");

        services.AddDbContext<AuthDbContext>((ctx, builder) =>
        {
            var options = ctx.GetRequiredService<IOptions<DatabaseOptions>>()
                             .Value;

            builder.UseNpgsql(options.Connection);

            if (options.EnableLogs)
            {
                builder.EnableDetailedErrors()
                       .EnableSensitiveDataLogging();
            }
        });
        
        services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
        {
            builder.AllowAnyOrigin()
                   .AllowAnyMethod()
                   .AllowAnyHeader();
        }));

        services.AddOcelot();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        InitializeDatabase(app);
        
        app.UseApiExceptionHandler();

        if (env.IsDevelopment()) app.UseSwaggerPage();

        app.UseRouting();
        app.UseCors("CorsPolicy");

        app.UseAuthentication();
        app.UseAuthorization();
        app.UseHttpLogging();

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

        app.UseOcelot().Wait();
    }
    
    private static void InitializeDatabase(IApplicationBuilder app)
    {
        using var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();

        scope.ServiceProvider.GetRequiredService<AuthDbContext>().Database.Migrate();
    }

}