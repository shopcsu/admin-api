using Shop.Auth;

Host.CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(x =>
     {
         x.UseStartup<Startup>();
         x.ConfigureAppConfiguration(c => c.AddJsonFile("routing.json"));
     })
    .ConfigureLogging(x => x.AddConsole())
    .Build()
    .Run();