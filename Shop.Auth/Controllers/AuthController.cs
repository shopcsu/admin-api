using System.Net;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Shop.Auth.Database;
using Shop.Auth.Dtos;
using Shop.Auth.Jwt;
using Shop.Auth.Options;
using Shop.Auth.Requests;
using Shop.Common.Extensions.Exceptions;
using Shop.Domain.Enums;
using Shop.Domain.Models;

namespace Shop.Auth.Controllers;

[Route("auth")]
[ApiController, AllowAnonymous]
[Consumes("application/json"), Produces("application/json")]
public class AuthController : ControllerBase
{
    private readonly AuthDbContext _dbContext;
    private readonly JwtService _jwt;
    private readonly AuthOptions _auth;

    public AuthController(AuthDbContext dbContext, JwtService jwt, IOptions<AuthOptions> auth)
    {
        _dbContext = dbContext;
        _jwt = jwt;
        _auth = auth.Value;
    }

    [HttpPost]
    public async Task<TokenDto> LogIn([FromBody] AuthRequest request, CancellationToken cancellationToken)
    {
        var user = await _dbContext.Users
                                   .Where(x => x.Name == request.Name)
                                   .Include(x => x.Tokens)
                                   .FirstOrDefaultAsync(cancellationToken)
                   ?? throw new RestException("User not found", HttpStatusCode.NotFound);

        if (user.Hash != CalcHash(request.Password))
        {
            throw new RestException("Incorrect password", HttpStatusCode.Forbidden);
        }

        request.IpAddress = HttpContext.Request.Headers.ContainsKey("X-Real-IP")
            ? IPAddress.Parse(HttpContext.Request.Headers["X-Real-IP"])
            : HttpContext.Connection.RemoteIpAddress;
        
        request.UserAgent = Request.Headers.UserAgent;
        
        var tokens = _jwt.GenerateToken(user, request);

        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return tokens;
    }

    [HttpPost("signup")]
    public async Task<TokenDto> SignUp([FromBody] AuthRequest request, CancellationToken cancellationToken)
    {
        var isTaken = await _dbContext.Users.Where(x => x.Name == request.Name).AnyAsync(cancellationToken);

        if (isTaken)
        {
            throw new RestException("This login is taken", HttpStatusCode.Forbidden);
        }
        
        if (request.Password == null || request.Password.Length < 4)
        {
            throw new RestException("Password is too weak", HttpStatusCode.Forbidden);
        }

        var user = new User
                   {
                       Name = request.Name,
                       Role = Role.Customer,
                       Tokens = new List<Token>(),
                       Hash = CalcHash(request.Password)
                   };

        _dbContext.Add(user);
        var tokens = _jwt.GenerateToken(user, request);
        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return tokens;
    }

    [HttpPost("refresh")]
    public async Task<TokenDto> Refresh([FromBody] RefreshRequest request, CancellationToken cancellationToken)
    {
        var tokens = await _jwt.RefreshTokenAsync(request.RefreshToken, cancellationToken);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return tokens;
    }

    private string CalcHash(string password) =>
        Convert.ToBase64String(KeyDerivation.Pbkdf2(_auth.Pepper + password,
                                                    Encoding.ASCII.GetBytes(_auth.Salt),
                                                    KeyDerivationPrf.HMACSHA256,
                                                    100000,
                                                    256 / 8));
}