namespace Shop.Admin.Dtos;

public class CategoryDto
{
    public int Id { get; set; }
    public string Title { get; set; }
    public int? ParentId { get; set; }
}