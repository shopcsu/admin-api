using System.Net;
using System.Net.Mime;
using System.Security.Cryptography.Xml;
using System.Text.Json;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using RabbitMQ.Client;
using Shop.Admin.Database;
using Shop.Admin.Dtos;
using Shop.Common.Extensions.Amqp;
using Shop.Common.Extensions.Common;
using Shop.Common.Extensions.Exceptions;
using Shop.Domain.Models;
using Shop.Domain.Requests;
using Shop.Domain.Requests.Categories;
using Shop.Domain.Requests.Items;

namespace Shop.Admin.Controllers;

[Route("admin")]
[ApiController, AllowAnonymous]
[Consumes("application/json"), Produces("application/json")]
public class AdminController : ControllerBase
{
    #region Deps

    private readonly AdminDbContext _dbContext;
    private readonly IModel _amqp;
    private readonly AmqpOptions _options;
    private readonly ILogger<AdminController> _logger;

    public AdminController(AdminDbContext dbContext,
        IModel amqp,
        IOptions<AmqpOptions> options,
        ILogger<AdminController> logger)
    {
        _dbContext = dbContext;
        _amqp = amqp;
        _logger = logger;
        _options = options.Value;
    }

    #endregion

    #region Items

    [HttpGet("items")]
    public async Task<ICollection<ItemDto>> ListItems(CancellationToken cancellationToken) =>
        await _dbContext.Items
                        .ProjectToType<ItemDto>()
                        .ToListAsync(cancellationToken);

    [HttpGet("items/{id:int}")]
    public async Task<ItemDto> GetItem([FromRoute] int id,
                                       CancellationToken cancellationToken) =>
        await _dbContext.Items
                        .Where(x => x.Id == id)
                        .ProjectToType<ItemDto>()
                        .FirstOrDefaultAsync(cancellationToken)
        ?? throw new RestException("Item not found", HttpStatusCode.NotFound);

    [HttpPost("items")]
    public async Task<int> CreateItem([FromBody] CreateItemRequest request,
                                      CancellationToken cancellationToken)
    {
        if (request.CategoryId == 0)
        {
            request.CategoryId = null;
        }
        var item = request.Adapt<Item>();
        _dbContext.Add(item);
        await _dbContext.SaveChangesAsync(cancellationToken);
        Dispatch(request.Also(x => x.Id = item.Id));
        return item.Id;
    }

    [HttpPost("items/{id:int}")]
    public async Task<int> UpdateItem([FromRoute] int id,
                                      [FromBody] UpdateItemRequest request,
                                      CancellationToken cancellationToken)
    {
        if (request.CategoryId == 0)
        {
            request.CategoryId = null;
        }
        var item = await _dbContext.Items
                                   .FirstOrDefaultAsync(x => x.Id == id, cancellationToken)
                   ?? throw new RestException("Item not found", HttpStatusCode.NotFound);
        request.Adapt(item);
        await _dbContext.SaveChangesAsync(cancellationToken);
        Dispatch(request.Also(x => x.Id = id));
        return item.Id;
    }

    [HttpDelete("items/{id:int}")]
    public async Task DeleteItem([FromRoute] int id,
                                 CancellationToken cancellationToken)
    {
        var item = await _dbContext.Items
                                   .FirstOrDefaultAsync(x => x.Id == id, cancellationToken)
                   ?? throw new RestException("Item not found", HttpStatusCode.NotFound);
        _dbContext.Remove(item);
        await _dbContext.SaveChangesAsync(cancellationToken);
        Dispatch(new DeleteItemRequest{Id = id});
    }

    #endregion

    #region Categories

    [HttpGet("categories")]
    public async Task<ICollection<CategoryDto>> ListCategories(CancellationToken cancellationToken) =>
        await _dbContext.Categories
                        .ProjectToType<CategoryDto>()
                        .ToListAsync(cancellationToken);

    [HttpGet("categories/{id:int}")]
    public async Task<CategoryDto> GetCategory([FromRoute] int id,
                                               CancellationToken cancellationToken) =>
        await _dbContext.Categories
                        .Where(x => x.Id == id)
                        .ProjectToType<CategoryDto>()
                        .FirstOrDefaultAsync(cancellationToken)
        ?? throw new RestException("Category not found", HttpStatusCode.NotFound);

    [HttpPost("categories")]
    public async Task<int> CreateCategory([FromBody] CreateCategoryRequest request,
                                          CancellationToken cancellationToken)
    {
        var category = request.Adapt<Category>();
        _dbContext.Add(category);
        await _dbContext.SaveChangesAsync(cancellationToken);
        Dispatch(request.Also(x => x.Id = category.Id));
        return category.Id;
    }

    [HttpPost("categories/{id:int}")]
    public async Task<int> UpdateCategory([FromRoute] int id,
                                          [FromBody] UpdateCategoryRequest request,
                                          CancellationToken cancellationToken)
    {
        var category = await _dbContext.Categories
                           .FirstOrDefaultAsync(x => x.Id == id, cancellationToken)
                       ?? throw new RestException("Category not found", HttpStatusCode.NotFound);
        request.Adapt(category);
        await _dbContext.SaveChangesAsync(cancellationToken);
        Dispatch(request.Also(x => x.Id = id));
        return category.Id;
    }

    [HttpDelete("categories/{id:int}")]
    public async Task DeleteCategory([FromRoute] int id,
                                     CancellationToken cancellationToken)
    {
        var category = await _dbContext.Categories
                                       .FirstOrDefaultAsync(x => x.Id == id, cancellationToken)
                       ?? throw new RestException("Item not found", HttpStatusCode.NotFound);
        _dbContext.Remove(category);
        await _dbContext.SaveChangesAsync(cancellationToken);
        Dispatch(new DeleteCategoryRequest{Id = id});
    }

    #endregion

    private void Dispatch<T>(T request)
    {
        var props = _amqp.CreateBasicProperties()
            .Also(x => x.ContentType = MediaTypeNames.Application.Json)
            .Also(x => x.Timestamp = new AmqpTimestamp(DateTime.UtcNow.ToUnixTimestamp()));
        
        _logger.LogInformation("dispatch {Evt}", request.GetType().Name);

        _amqp.BasicPublish(_options.Exchange,
            request.GetType().Name,
            props,
            JsonSerializer.SerializeToUtf8Bytes(request)
        );
    }
}