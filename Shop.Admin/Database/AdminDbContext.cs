using Microsoft.EntityFrameworkCore;
using Shop.Domain.Models;

namespace Shop.Admin.Database;

public class AdminDbContext : DbContext
{
    public DbSet<Category> Categories { get; set; }
    public DbSet<Item> Items { get; set; }
    
    public AdminDbContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Category>()
                    .HasOne(x => x.Parent)
                    .WithMany(x => x.Children)
                    .HasForeignKey(x => x.ParentId);
        
        base.OnModelCreating(modelBuilder);
    }
}