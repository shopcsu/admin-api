using Mapster;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Shop.Admin.Database;
using Shop.Admin.Options;
using Shop.Common.Extensions.Amqp;
using Shop.Common.Extensions.Exceptions;
using Shop.Common.Extensions.Json;
using Shop.Common.Extensions.Swagger;
using Shop.Domain.Models;
using Shop.Domain.Requests.Categories;
using Shop.Domain.Requests.Items;

namespace Shop.Admin;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSwagger();
        services.AddControllers()
                .AddJsonConfig();

        services.AddJsonConfig();
        
        services.AddOptions<DatabaseOptions>()
                .BindConfiguration("Database");

        services.AddDbContext<AdminDbContext>((ctx, builder) =>
        {
            var options = ctx.GetRequiredService<IOptions<DatabaseOptions>>()
                             .Value;

            builder.UseNpgsql(options.Connection);

            if (options.EnableLogs)
            {
                builder.EnableDetailedErrors()
                       .EnableSensitiveDataLogging();
            }
        });
        
        services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
        {
            builder.AllowAnyOrigin()
                   .AllowAnyMethod()
                   .AllowAnyHeader();
        }));

        services.AddAmqp();
        
        ConfigureMapper(TypeAdapterConfig.GlobalSettings);
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        InitializeDatabase(app);
        
        app.UseApiExceptionHandler();

        if (env.IsDevelopment()) app.UseSwaggerPage();

        app.UseRouting();
        app.UseCors("CorsPolicy");

        app.UseAuthentication();
        app.UseAuthorization();
        app.UseHttpLogging();

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
    
    private static void InitializeDatabase(IApplicationBuilder app)
    {
        using var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();

        scope.ServiceProvider.GetRequiredService<AdminDbContext>().Database.Migrate();
    }

    private static void ConfigureMapper(TypeAdapterConfig cfg)
    {
        cfg.NewConfig<CreateCategoryRequest, Category>()
            .Ignore(x => x.Id);
        
        cfg.NewConfig<UpdateCategoryRequest, Category>()
            .Ignore(x => x.Id);
        
        cfg.NewConfig<CreateItemRequest, Item>()
            .Ignore(x => x.Id);
        
        cfg.NewConfig<UpdateItemRequest, Item>()
            .Ignore(x => x.Id);
    }
}