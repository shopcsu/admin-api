namespace Shop.Domain.Enums;

public enum Role
{
    Admin,
    Customer
}