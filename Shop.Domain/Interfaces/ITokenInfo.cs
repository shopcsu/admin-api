using System.Net;

namespace Shop.Domain.Interfaces;

public interface ITokenInfo
{
    public IPAddress IpAddress { get; set; }
    public string UserAgent { get; set; }
}