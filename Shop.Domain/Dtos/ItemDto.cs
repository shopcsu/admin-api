namespace Shop.Domain.Dtos;

public class ItemDto
{
    public int Id { get; set; }
    public string Title { get; set; }
    public int Price { get; set; }
    public int Quantity { get; set; }
}