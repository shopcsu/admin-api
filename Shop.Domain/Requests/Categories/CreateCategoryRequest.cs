using MediatR;

namespace Shop.Domain.Requests.Categories;

public class CreateCategoryRequest : IRequest
{
    public int Id { get; set; }
    public string Title { get; set; }
    public int? ParentId { get; set; }
}