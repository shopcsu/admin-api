using MediatR;

namespace Shop.Domain.Requests.Items;

public class DeleteItemRequest : IRequest
{
    public int Id { get; set; }
}