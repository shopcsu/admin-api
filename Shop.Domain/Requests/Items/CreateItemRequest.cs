using MediatR;

namespace Shop.Domain.Requests.Items;

public class CreateItemRequest : IRequest
{
    public int Id { get; set; }
    public int? CategoryId { get; set; }
    public string Title { get; set; }
    public int Price { get; set; }
    public string Picture { get; set; }
    public string Description { get; set; }
}