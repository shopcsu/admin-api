﻿using MediatR;
using Shop.Domain.Enums;

namespace Shop.Domain.Requests.Orders;

public class ChangeOrderStatusRequest : IRequest
{
    public Guid Id { get; set; }
    public int UserId { get; set; }
    public OrderStatus Status { get; set; }
    public Role Role { get; set; }
}