﻿using MediatR;
using Shop.Domain.Dtos;

namespace Shop.Domain.Requests.Orders;

public class MakeOrderRequest : IRequest
{
    public int UserId { get; set; }
    public CartDto Cart { get; set; }
    public DateTime Created { get; set; }
}