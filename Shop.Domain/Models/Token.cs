using System.Net;

namespace Shop.Domain.Models;

public class Token
{
    public int Id { get; set; }
    public Guid Value { get; set; } = Guid.NewGuid();

    public int UserId { get; set; }
    public User User { get; set; }

    public IPAddress IpAddress { get; set; }
    public string UserAgent { get; set; }
}