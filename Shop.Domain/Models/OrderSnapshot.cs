﻿namespace Shop.Domain.Models;

public class OrderSnapshot
{
    public int Id { get; set; }
    public Guid AggregateId { get; set; }
    public OrderAggregate Aggregate { get; set; }
    public string Snapshot { get; set; }
    public int Version { get; set; }
}