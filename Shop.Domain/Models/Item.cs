namespace Shop.Domain.Models;

public class Item
{
    public int Id { get; set; }

    public int? CategoryId { get; set; }
    public Category Category { get; set; }
    
    public string Title { get; set; }
    public int Price { get; set; }
    public string Picture { get; set; }
    public string Description { get; set; }
}