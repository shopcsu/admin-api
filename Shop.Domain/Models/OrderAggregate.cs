﻿namespace Shop.Domain.Models;

public class OrderAggregate
{
    public Guid Id { get; set; }
    public string Type { get; set; }
    public int OwnerId { get; set; }
    public int Version { get; set; }

    public ICollection<OrderSnapshot> Snapshots { get; set; } = new List<OrderSnapshot>();
    public ICollection<OrderEvent> Events { get; set; } = new List<OrderEvent>();
}