﻿namespace Shop.Domain.Models;

public class OrderEvent
{
    public int Id { get; set; }
    public Guid AggregateId { get; set; }
    public OrderAggregate Aggregate { get; set; }
    public string EventData { get; set; }
    public int Version { get; set; }
}