using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Shop.Admin.Options;
using Shop.Common.Extensions.Amqp;
using Shop.Common.Extensions.Exceptions;
using Shop.Orders.Database;

var builder = WebApplication.CreateBuilder(args);

builder.Services
    .AddOptions<DatabaseOptions>()
    .BindConfiguration("Database");

builder.Services.AddEndpointsApiExplorer()
    .AddSwaggerGen()
    .AddAmqp()
    .AddDbContext<OrdersContext>((ctx, b) =>
    {
        var options = ctx.GetRequiredService<IOptions<DatabaseOptions>>()
            .Value;

        b.UseNpgsql(options.Connection);

        if (options.EnableLogs)
        {
            b.EnableDetailedErrors()
                .EnableSensitiveDataLogging();
        }
    })
    .AddControllers();

var app = builder.Build();

InitializeDatabase(app);

app.UseApiExceptionHandler();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

static void InitializeDatabase(IApplicationBuilder app)
{
    using var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
    scope.ServiceProvider.GetRequiredService<OrdersContext>().Database.Migrate();
}