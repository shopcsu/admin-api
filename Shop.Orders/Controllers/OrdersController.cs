using System.Net;
using System.Net.Mime;
using System.Text.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using Shop.Common.Extensions.Amqp;
using Shop.Common.Extensions.Common;
using Shop.Common.Extensions.Exceptions;
using Shop.Domain.Dtos;
using Shop.Domain.Enums;
using Shop.Domain.Requests.Orders;
using Shop.Orders.Database;

namespace Shop.Orders.Controllers;

[Route("orders")]
[ApiController, AllowAnonymous]
[Consumes("application/json"), Produces("application/json")]
public class OrdersController : ControllerBase
{
    private readonly OrdersContext _context;
    private readonly IModel _model;
    private readonly AmqpOptions _options;

    public OrdersController(OrdersContext context, IOptions<AmqpOptions> options, IModel model)
    {
        _context = context;
        _model = model;
        _options = options.Value;
    }

    [HttpGet]
    public List<OrderDto> GetOrders()
    {
        var role = Enum.Parse<Role>(HttpContext.Request.Headers["Role"]);
        if (role == Role.Admin)
        {
            return _context.Aggregates
                .Include(x => x.Snapshots)
                .ToList()
                .Select(x => x.Snapshots.MaxBy(s => s.Version))
                .Select(x => JsonSerializer.Deserialize<OrderDto>(x.Snapshot))
                .ToList();
        }

        var id = int.Parse(HttpContext.Request.Headers["Id"]);
        return _context.Aggregates
            .Where(x => x.OwnerId == id)
            .Include(x => x.Snapshots)
            .ToList()
            .Select(x => x.Snapshots.MaxBy(s => s.Version))
            .Select(x => JsonSerializer.Deserialize<OrderDto>(x.Snapshot))
            .ToList();
    }

    [HttpPatch("{status}/{id:guid}")]
    public async Task ChangeOrderStatus(string status, Guid id)
    {
        var role = Enum.Parse<Role>(HttpContext.Request.Headers["Role"]);
        if (!Enum.TryParse<OrderStatus>(status, out var enumStatus))
        {
            throw new RestException("Invalid status", HttpStatusCode.BadRequest);
        }

        var uid = int.Parse(HttpContext.Request.Headers["Id"]);

        var props = _model.CreateBasicProperties()
            .Also(x => x.ContentType = MediaTypeNames.Application.Json)
            .Also(x => x.Timestamp = new AmqpTimestamp(DateTime.UtcNow.ToUnixTimestamp()));

        var request = new ChangeOrderStatusRequest
        {
            Id = id,
            Role = role,
            Status = enumStatus,
            UserId = uid
        };

        _model.BasicPublish(_options.Exchange,
            request.GetType().Name,
            props,
            JsonSerializer.SerializeToUtf8Bytes(request)
        );
    }
}