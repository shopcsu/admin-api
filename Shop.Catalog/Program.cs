using Shop.Catalog;

Host.CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(x =>
     {
         x.UseStartup<Startup>();
     })
    .Build()
    .Run();