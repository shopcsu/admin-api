namespace Shop.Catalog.Dtos;

public class ChildCategoryDto
{
    public int Id { get; set; }
    public string Title { get; set; }
}