namespace Shop.Catalog.Dtos;

public class CategoryDto
{
    public int Id { get; set; }
    public string Title { get; set; }
    public ICollection<ChildCategoryDto> Children { get; set; } = new List<ChildCategoryDto>();
    public ICollection<ItemDto> Items { get; set; } = new List<ItemDto>();
}