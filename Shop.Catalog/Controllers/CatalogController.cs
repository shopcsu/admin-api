using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Shop.Catalog.Database;
using Shop.Catalog.Dtos;
using Shop.Domain.Models;

namespace Shop.Catalog.Controllers;

[Route("catalog")]
[ApiController, AllowAnonymous]
[Consumes("application/json"), Produces("application/json")]
public class CatalogController : ControllerBase
{
    private readonly MongoReader _mongoReader;
    
    public CatalogController(MongoReader mongoReader)
    {
        _mongoReader = mongoReader;
    }

    [HttpGet("categories/{id:int?}")]
    public async Task<CategoryDto> Get(int? id)
    {
        return await _mongoReader.GetCategory(id ?? -1);
    }

    [HttpGet("reinit")]
    public async Task Reinit()
    {
        await _mongoReader.Reinit();
    }
}