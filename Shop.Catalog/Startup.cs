using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Shop.Catalog.Database;
using Shop.Common.Extensions.Exceptions;
using Shop.Common.Extensions.Json;
using Shop.Common.Extensions.Mongo;
using Shop.Common.Extensions.Swagger;

namespace Shop.Catalog;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSwagger();
        services.AddControllers()
                .AddJsonConfig();

        services.AddJsonConfig();

        services.AddMongo();
        services.AddSingleton<MongoReader>();
        
        services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
        {
            builder.AllowAnyOrigin()
                   .AllowAnyMethod()
                   .AllowAnyHeader();
        }));
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        InitializeDatabase(app);
        
        app.UseApiExceptionHandler();

        if (env.IsDevelopment()) app.UseSwaggerPage();

        app.UseRouting();
        app.UseCors("CorsPolicy");

        app.UseAuthentication();
        app.UseAuthorization();
        app.UseHttpLogging();

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
    
    private static void InitializeDatabase(IApplicationBuilder app)
    {
        using var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();

        //scope.ServiceProvider.GetRequiredService<AdminDbContext>().Database.Migrate();
    }
}