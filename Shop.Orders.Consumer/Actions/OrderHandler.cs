﻿using System.Text.Json;
using MediatR;
using Shop.Domain.Dtos;
using Shop.Domain.Enums;
using Shop.Domain.Models;
using Shop.Domain.Requests.Orders;
using Shop.Orders.Consumer.Database;

namespace Shop.Orders.Consumer.Actions;

public class OrderHandler : IRequestHandler<MakeOrderRequest>
{
    private readonly OrdersContext _context;

    public OrderHandler(OrdersContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(MakeOrderRequest request, CancellationToken cancellationToken)
    {
        var oid = Guid.NewGuid();
        
        var order = new OrderDto
        {
            Id = oid,
            Created = request.Created,
            Items = request.Cart.Items,
            Status = OrderStatus.NotConfirmed,
            TotalPrice = request.Cart.TotalPrice
        };

        var aggregate = new OrderAggregate
        {
            Id = oid,
            OwnerId = request.UserId,
            Version = 1
        };
        
        aggregate.Snapshots.Add(new OrderSnapshot
        {
            Snapshot = JsonSerializer.Serialize(order),
            Version = 1
        });

        _context.Aggregates.Add(aggregate);

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}