﻿using System.Text.Json;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shop.Domain.Dtos;
using Shop.Domain.Enums;
using Shop.Domain.Models;
using Shop.Domain.Requests.Orders;
using Shop.Orders.Consumer.Database;

namespace Shop.Orders.Consumer.Actions;

public class StatusHandler : IRequestHandler<ChangeOrderStatusRequest>
{
    private readonly OrdersContext _context;

    public StatusHandler(OrdersContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(ChangeOrderStatusRequest request, CancellationToken cancellationToken)
    {
        OrderAggregate aggregate;

        if (request.Role == Role.Admin)
        {
            aggregate = await _context.Aggregates
                .Include(x => x.Snapshots)
                .Include(x => x.Events)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
        }
        else
        {
            aggregate = await _context.Aggregates
                .Where(x => x.OwnerId == request.UserId)
                .Include(x => x.Snapshots)
                .Include(x => x.Events)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
        }

        if (aggregate is null) return Unit.Value;

        var snap = JsonSerializer.Deserialize<OrderDto>(aggregate.Snapshots.MaxBy(x => x.Version)!.Snapshot);

        if (request.Role == Role.Customer &&
            request.Status != OrderStatus.Canceled
            ||
            snap!.Status == OrderStatus.Canceled
            ||
            request.Role == Role.Admin &&
            request.Status == OrderStatus.Finished &&
            snap.Status != OrderStatus.Confirmed)
        {
            return Unit.Value;
        }

        snap!.Status = request.Status;

        aggregate.Version++;
        aggregate.Events.Add(new OrderEvent
        {
            Version = aggregate.Version,
            EventData = request.Status.ToString()
        });
        aggregate.Snapshots.Add(new OrderSnapshot
        {
            Version = aggregate.Version,
            Snapshot = JsonSerializer.Serialize(snap)
        });

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}