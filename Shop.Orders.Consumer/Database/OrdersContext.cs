﻿using Microsoft.EntityFrameworkCore;
using Shop.Domain.Models;

namespace Shop.Orders.Consumer.Database;

public class OrdersContext : DbContext
{
    public DbSet<OrderAggregate> Aggregates { get; set; }
    public DbSet<OrderSnapshot> Snapshots { get; set; }
    public DbSet<OrderEvent> Events { get; set; }

    public OrdersContext(DbContextOptions options) : base(options)
    {
    }
}