namespace Shop.Orders.Consumer.Options;

public class DatabaseOptions
{
    public string Connection { get; set; }
    public bool EnableLogs { get; set; }
}