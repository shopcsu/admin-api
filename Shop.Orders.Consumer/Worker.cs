using System.Text;
using System.Text.Json;
using MediatR;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Shop.Common.Extensions.Amqp;
using Shop.Domain.Requests.Items;
using Shop.Domain.Requests.Orders;

namespace Shop.Orders.Consumer;

public class Worker : IHostedService
{
    private readonly ILogger<Worker> _logger;
    private readonly IModel _model;
    private readonly AmqpOptions _options;
    private readonly IMediator _mediator;

    public Worker(ILogger<Worker> logger, IOptions<AmqpOptions> options, IModel model, IMediator mediator)
    {
        _logger = logger;
        _options = options.Value;
        _model = model;
        _mediator = mediator;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _model.ExchangeDeclare(_options.Exchange, ExchangeType.Fanout);
        _model.QueueDeclare(_options.Queue, exclusive: false);
        
        _model.QueueBind(_options.Queue, _options.Exchange, nameof(ChangeOrderStatusRequest));
        _model.QueueBind(_options.Queue, _options.Exchange, nameof(MakeOrderRequest));
        
        _logger.LogInformation("Queue: {Queue}", _options.Queue);

        var consumer = new EventingBasicConsumer(_model);
        consumer.Received += async (sender, evt) =>
        {
            var json = Encoding.UTF8.GetString(evt.Body.ToArray());
            _logger.LogInformation("Dispatching {Key}", json);
            
            var type = typeof(CreateItemRequest).Assembly.GetTypes().First(t => t.Name == evt.RoutingKey);

            var request = JsonSerializer.Deserialize(json, type)!;
            
            await _mediator.Send(request);
        };

        _model.BasicConsume(_options.Queue, consumer: consumer, autoAck: true);

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken) =>
        Task.CompletedTask;
}