using System.Text.Json;
using MediatR;
using Shop.Cart.Consumer.Dtos;
using Shop.Common.Extensions.Common;
using Shop.Domain.Requests.Items;
using StackExchange.Redis;

namespace Shop.Cart.Consumer.Actions;

public class ItemHandler : IRequestHandler<UpdateItemRequest>, IRequestHandler<DeleteItemRequest>
{
    private readonly IConnectionMultiplexer _redis;

    public ItemHandler(IConnectionMultiplexer redis)
    {
        _redis = redis;
    }

    public async Task<Unit> Handle(UpdateItemRequest request, CancellationToken cancellationToken)
    {
        var db = _redis.GetDatabase();
        var keys = _redis
            .GetServer(_redis
                .GetEndPoints()
                .First())
            .Keys(pattern: "*");

        foreach (var key in keys)
        {
            var cached = await db.StringGetAsync(key);
            var cart = cached.HasValue ? JsonSerializer.Deserialize<CartDto>(cached) : null;

            var item = cart?.Items.FirstOrDefault(x => x.Id == request.Id);

            if (item == null || cart == null) continue;

            cart.Items.Remove(item);
            cart.Items.Add(item
                .Also(x => x.Title = request.Title)
                .Also(x => x.Price = x.Quantity * request.Price));
            
            cart.TotalPrice = cart.Items.Select(x => x.Price).Sum();
            
            await db.StringSetAsync(key, JsonSerializer.Serialize(cart), TimeSpan.FromDays(2));
        }

        return Unit.Value;
    }

    public async Task<Unit> Handle(DeleteItemRequest request, CancellationToken cancellationToken)
    {
        var db = _redis.GetDatabase();
        var keys = _redis
            .GetServer(_redis
                .GetEndPoints()
                .First())
            .Keys(pattern: "*");

        foreach (var key in keys)
        {
            var cached = await db.StringGetAsync(key);
            var cart = cached.HasValue ? JsonSerializer.Deserialize<CartDto>(cached) : null;

            if (cart is null || cart.Items.All(x => x.Id != request.Id)) continue;

            cart.Items = cart.Items.Where(x => x.Id != request.Id).ToList();
            cart.TotalPrice = cart.Items.Select(x => x.Price).Sum();
            await db.StringSetAsync(key, JsonSerializer.Serialize(cart), TimeSpan.FromDays(2));
        }

        return Unit.Value;
    }
}