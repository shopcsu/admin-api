namespace Shop.Cart.Consumer.Dtos;

public class CartDto
{
    public int TotalPrice { get; set; }
    public ICollection<ItemDto> Items { get; set; } = new List<ItemDto>();
}