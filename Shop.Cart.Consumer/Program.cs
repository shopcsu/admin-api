using System.Reflection;
using MediatR;
using Shop.Cart.Consumer;
using Shop.Common.Extensions.Amqp;
using Shop.Common.Extensions.Json;
using Shop.Common.Extensions.Redis;

var host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddMediatR(Assembly.GetExecutingAssembly());
        services.AddRedis();
        services.AddAmqp();
        services.AddJsonConfig();
        services.AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();