using Microsoft.Extensions.DependencyInjection;

namespace Shop.Common.Extensions.Context;

public static class ContextExtensions
{
    public static IServiceCollection AddRequestContext(this IServiceCollection services)
    {
        services.AddScoped<RequestContext>()
                .AddScoped<IContext>(x => x.GetRequiredService<RequestContext>());
        
        services.AddScoped<ContextMiddleware>();
        return services;
    }
}