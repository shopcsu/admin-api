using Shop.Domain.Enums;

namespace Shop.Common.Extensions.Context;

public class RequestContext : IContext
{
    public bool IsAuthenticated { get; set; }
    public int UserId { get; set; }
    public Role Role { get; set; }
}