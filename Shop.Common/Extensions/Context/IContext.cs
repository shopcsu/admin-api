using Shop.Domain.Enums;

namespace Shop.Common.Extensions.Context;

public interface IContext
{
    public bool IsAuthenticated { get; }
    public int UserId { get; }
    public Role Role { get; }
}