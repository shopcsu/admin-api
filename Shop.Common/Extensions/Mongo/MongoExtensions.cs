using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Shop.Common.Extensions.Mongo;

public static class MongoExtensions
{
    public static IServiceCollection AddMongo(this IServiceCollection services)
    {
        services.AddOptions<MongoConnection>()
            .BindConfiguration("Mongo");

        services.AddSingleton<IMongoClient>(ctx =>
        {
            var options = ctx.GetRequiredService<IOptions<MongoConnection>>()
                .Value;

            return new MongoClient(options.ConnectionString);
        });
        
        services.AddSingleton(ctx =>
        {
            var options = ctx.GetRequiredService<IOptions<MongoConnection>>()
                .Value;
            var client = ctx.GetRequiredService<IMongoClient>();
            return client.GetDatabase(options.Database);
        });
        
        return services;
    }
}