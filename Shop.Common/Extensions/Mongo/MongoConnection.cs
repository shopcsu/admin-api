namespace Shop.Common.Extensions.Mongo;

public class MongoConnection
{
    public string ConnectionString { get; set; }
    public string Database { get; set; }
}