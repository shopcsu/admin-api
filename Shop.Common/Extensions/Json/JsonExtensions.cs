﻿using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Http.Json;
using Microsoft.Extensions.DependencyInjection;

namespace Shop.Common.Extensions.Json;

public static class JsonExtensions
{
    public static IServiceCollection AddJsonConfig(this IServiceCollection services)
    {
        services.AddOptions<JsonOptions>()
                .Configure(options => ConfigureJsonSerializer(options.SerializerOptions));

        services.AddOptions<JsonSerializerOptions>()
                .Configure(ConfigureJsonSerializer);
        
        ((JsonSerializerOptions)typeof(JsonSerializerOptions)
                .GetField("s_defaultOptions", 
                    System.Reflection.BindingFlags.Static |
                    System.Reflection.BindingFlags.NonPublic)!.GetValue(null))!
            .PropertyNameCaseInsensitive = true;

        return services;
    }
    
    public static IMvcBuilder AddJsonConfig(this IMvcBuilder builder) =>
        builder.AddJsonOptions(options => ConfigureJsonSerializer(options.JsonSerializerOptions));

    private static void ConfigureJsonSerializer(JsonSerializerOptions options)
    {
        options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
        options.Converters.Add(new JsonStringEnumConverter(options.PropertyNamingPolicy));
        options.Converters.Add(new DateOnlyConverter());
        options.Converters.Add(new TimeOnlyConverter());
    }
}