﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Shop.Common.Extensions.Swagger;

public static class SwaggerExtensions
{
    public static IServiceCollection AddSwagger(this IServiceCollection services) =>
        services.AddSwaggerGen(ConfigureSwagger);

    public static IApplicationBuilder UseSwaggerPage(this IApplicationBuilder app) =>
        app.UseSwagger(ConfigureSwagger)
           .UseSwaggerUI(ConfigureSwagger);

    private static void ConfigureSwagger(SwaggerOptions c)
    {
        c.PreSerializeFilters.Add((swagger, httpReq) =>
        {
            swagger.Servers = new List<OpenApiServer>
            {
                new() { Url = "http://localhost:5000" }
            };
            var thisServer = new OpenApiServer
            {
                Url = $"{(httpReq.Host.Value.Contains("localhost") ? httpReq.Scheme : "https")}://{httpReq.Host.Value}"
            };
            swagger.Servers.Remove(thisServer);
            swagger.Servers.Insert(0, thisServer);
        });
    }

    private static void ConfigureSwagger(SwaggerGenOptions c)
    {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
            Title = "Shop",
            Version = "v1.0"
        });
        
        c.AddCustomTypes();

        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            Name = "Authorization",
            Type = SecuritySchemeType.Http,
            In = ParameterLocation.Header,
            Scheme = "bearer",
            BearerFormat = "JWT",
            Description = "JWT Authorization header using the Bearer scheme."
        });

        c.CustomOperationIds(api => api.TryGetMethodInfo(out var methodInfo) ? methodInfo.Name : null);
        c.CustomSchemaIds(x => x.FullName);

        c.UseOneOfForPolymorphism();
        c.UseAllOfForInheritance();
        c.UseInlineDefinitionsForEnums();
        c.UseAllOfToExtendReferenceSchemas();

        c.OperationFilter<NotMappedSwaggerFilter>();
        c.OperationFilter<AuthSwaggerFilter>();
        c.SchemaFilter<NotMappedSwaggerFilter>();
    }

    private static void ConfigureSwagger(SwaggerUIOptions c)
    {
        c.DocumentTitle = "Shop";
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Shop");
        c.InjectStylesheet(
            "https://rawcdn.githack.com/Amoenus/SwaggerDark/5106ba4b198fd5f9fbde7ea4bca248f9ff55ac6a/SwaggerDark.css");
    }

    private static void AddCustomTypes(this SwaggerGenOptions c)
    {
        c.MapType<DateOnly>(() => new OpenApiSchema
        {
            Type = "string",
            Format = "date"
        });
        c.MapType<TimeOnly>(() => new OpenApiSchema
        {
            Type = "string",
            Format = "time",
            Pattern = @"^\d{2}:\d{2}$",
            Example = new OpenApiString("14:52")
        });
    }
}