﻿namespace Shop.Common.Extensions.Exceptions;

public class ExceptionDto
{
    public int Code { get; set; }
    public string Type { get; set; }
    public string Message { get; set; }
}