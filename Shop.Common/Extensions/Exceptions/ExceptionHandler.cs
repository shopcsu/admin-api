﻿using System.Net;
using Microsoft.AspNetCore.Http;

namespace Shop.Common.Extensions.Exceptions;

public class ExceptionHandler
{
    public (HttpStatusCode Code, object Body) Handle(Exception exception, HttpContext context)
    {
        return exception switch
        {
            RestException restException => (restException.Code,
                                            new ExceptionDto
                                            {
                                                Code = (int)restException.Code,
                                                Type = restException.Type,
                                                Message = restException.Message
                                            }),

            NotImplementedException => (HttpStatusCode.NotImplemented,
                                        new ExceptionDto
                                        {
                                            Code = (int)HttpStatusCode.NotImplemented,
                                            Type = "not implemented",
                                            Message = "Метод еще не реализован"
                                        }),

            _ => (HttpStatusCode.InternalServerError,
                  new ExceptionDto
                  {
                      Code = (int)HttpStatusCode.InternalServerError,
                      Type = exception.GetType().ToString(),
                      Message = exception.Message
                  })
        };
    }

    public bool Report(HttpStatusCode status, Exception exception, HttpRequest request) =>
        status == HttpStatusCode.InternalServerError;
}