﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;

namespace Shop.Common.Extensions.Amqp;

public static class AmqpExtensions
{
    public static IServiceCollection AddAmqp(this IServiceCollection services, string configurationSection = "RabbitMQ")
    {
        services.AddOptions<AmqpOptions>()
                .BindConfiguration(configurationSection)
                .ValidateDataAnnotations();

        services.AddSingleton<IConnectionFactory>(x =>
        {
            var options = x.GetRequiredService<IOptions<AmqpOptions>>()
                           .Value;

            return new ConnectionFactory
            {
                HostName = options.Host,
                UserName = options.Username,
                Password = options.Password,
                Port = options.Port,
                RequestedHeartbeat = options.HeartbeatTimeout
            };
        });

        services.AddSingleton(x => x.GetRequiredService<IConnectionFactory>()
                                                 .CreateConnection());

        services.AddSingleton(x => x.GetRequiredService<IConnection>()
                                            .CreateModel());

        return services;
    }
}