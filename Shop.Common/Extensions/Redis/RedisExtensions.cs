using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using StackExchange.Redis;

namespace Shop.Common.Extensions.Redis;

public static class RedisExtensions
{
    public static IServiceCollection AddRedis(this IServiceCollection services)
    {
        services.AddOptions<RedisOptions>()
            .BindConfiguration("Redis");
        services.AddSingleton<IConnectionMultiplexer>(ctx =>
            ConnectionMultiplexer.Connect(ctx.GetRequiredService<IOptions<RedisOptions>>().Value.ConnectionString));
        return services;
    }
}