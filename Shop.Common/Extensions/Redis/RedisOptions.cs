namespace Shop.Common.Extensions.Redis;

public class RedisOptions
{
    public string ConnectionString { get; set; }
}