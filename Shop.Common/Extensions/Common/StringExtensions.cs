﻿using System.Security.Cryptography;
using System.Text;

namespace Shop.Common.Extensions.Common;

public static class StringExtensions
{
    public static string Capitalize(this string s) =>
        string.Create(s.Length, s, (chars, str) =>
        {
            chars[0] = char.ToUpper(str[0]);

            for (var i = 1; i < chars.Length; i++)
            {
                chars[i] = str[i];
            }
        });

    public static string Uncapitalize(this string s) =>
        string.Create(s.Length, s, (chars, str) =>
        {
            chars[0] = char.ToLower(str[0]);

            for (var i = 1; i < chars.Length; i++)
            {
                chars[i] = str[i];
            }
        });
    
    public static string ToHexString(this byte[] bytes) =>
        string.Create(bytes.Length * 2, bytes, (span, array) =>
        {
            for (var i = 0; i < array.Length; i++)
            {
                var b = (byte)(array[i] >> 4);
                span[i * 2] = (char)(b > 9 ? b + 0x37 : b + 0x30);
                b = (byte)(array[i] & 0xF);
                span[i * 2 + 1] = (char)(b > 9 ? b + 0x37 : b + 0x30);
            }
        });

    public static string CreateHmacToken(this string message, string secret)
    {
        var encoding = new UTF8Encoding();
        var keyByte = encoding.GetBytes(secret);
        var messageBytes = encoding.GetBytes(message);
        using var hmacSha256 = new HMACSHA256(keyByte);

        var hashMessage = hmacSha256.ComputeHash(messageBytes);
        return Convert.ToBase64String(hashMessage);
    }
}