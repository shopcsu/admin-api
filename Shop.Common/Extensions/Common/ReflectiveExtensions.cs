using System.Reflection;

namespace Shop.Common.Extensions.Common;

public static class ReflectiveExtensions
{
    public static IEnumerable<Type> GetImplementationsFor<T>(this Assembly assembly) =>
        assembly.GetTypes()
                .Where(t => !t.IsAbstract && t.IsAssignableTo(typeof(T)));
}