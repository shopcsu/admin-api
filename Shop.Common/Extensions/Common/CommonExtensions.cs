using System.Runtime.CompilerServices;

namespace Shop.Common.Extensions.Common;

public static class CommonExtensions
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T As<T>(this object obj) =>
        (T)obj;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T Also<T>(this T item, Action<T> func)
    {
        func(item);
        return item;
    }
}