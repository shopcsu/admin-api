using MediatR;
using MongoDB.Driver;
using Shop.Catalog.Consumer.Dtos;
using Shop.Domain.Models;
using Shop.Domain.Requests.Categories;

namespace Shop.Catalog.Consumer.Actions;

public class CategoryHandler
    : IRequestHandler<CreateCategoryRequest>,
        IRequestHandler<UpdateCategoryRequest>,
        IRequestHandler<DeleteCategoryRequest>
{
    private readonly ILogger<CategoryHandler> _logger;
    private readonly IMongoDatabase _database;

    public CategoryHandler(ILogger<CategoryHandler> logger, IMongoDatabase database)
    {
        _logger = logger;
        _database = database;
    }

    public async Task<Unit> Handle(CreateCategoryRequest request, CancellationToken cancellationToken)
    {
        var col = _database.GetCollection<CategoryDto>("categories");
        
        _logger.LogInformation("create category. {Pid} {Title} {Id}",
            request.ParentId, request.Title, request.Id);
        
        var child = new ChildCategoryDto
        {
            Id = request.Id,
            Title = request.Title
        };
        var category = new CategoryDto
        {
            Id = request.Id,
            Title = request.Title
        };

        var parent = request.ParentId ?? -1;
        var filter = Builders<CategoryDto>.Filter.Eq(x => x.Id, parent);
        var update = Builders<CategoryDto>.Update.Push(x => x.Children, child);
        var result = await col.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
        
        await col.InsertOneAsync(category, cancellationToken: cancellationToken);
        
        _logger.LogInformation("created category. found: {Match}; updated: {Mod}",
            result.MatchedCount, result.ModifiedCount);

        return Unit.Value;
    }

    public async Task<Unit> Handle(UpdateCategoryRequest request, CancellationToken cancellationToken)
    {
        var col = _database.GetCollection<CategoryDto>("categories");
        
        var child = new ChildCategoryDto
        {
            Id = request.Id,
            Title = request.Title
        };
        var category = new CategoryDto
        {
            Id = request.Id,
            Title = request.Title
        };

        var parent = request.ParentId ?? -1;
        var parentFilter = Builders<CategoryDto>.Filter.ElemMatch(x => x.Children, x => x.Id == request.Id);
        var f = Builders<ChildCategoryDto>.Filter.Eq(x => x.Id, request.Id);
        var parentUpdate = Builders<CategoryDto>.Update.PullFilter(x => x.Children, f);
        await col.UpdateOneAsync(parentFilter, parentUpdate, cancellationToken: cancellationToken);
        
        var filter = Builders<CategoryDto>.Filter.Eq(x => x.Id, parent);
        var update = Builders<CategoryDto>.Update.Push(x => x.Children, child);
        await col.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);

        var rootFilter = Builders<CategoryDto>.Filter.Eq(x => x.Id, request.Id);
        var rootUpdate = Builders<CategoryDto>.Update
            .Set(x => x.Title, request.Title);
        
        await col.UpdateOneAsync(rootFilter, rootUpdate, cancellationToken: cancellationToken);
        return Unit.Value;
    }

    public async Task<Unit> Handle(DeleteCategoryRequest request, CancellationToken cancellationToken)
    {
        var col = _database.GetCollection<CategoryDto>("categories");

        var deleted = await col.FindOneAndDeleteAsync(x => x.Id == request.Id, cancellationToken: cancellationToken);
        
        var parentFilter = Builders<CategoryDto>.Filter.ElemMatch(x => x.Children, x => x.Id == request.Id);
        var update = Builders<CategoryDto>.Update.Pull(x => x.Children, new ChildCategoryDto{Id = deleted.Id, Title = deleted.Title});
        await col.UpdateOneAsync(parentFilter, update, cancellationToken: cancellationToken);

        var rootUpdate = Builders<CategoryDto>.Update
            .PushEach(x => x.Children, deleted.Children)
            .PushEach(x => x.Items, deleted.Items);
        await col.UpdateOneAsync(x => x.Id == -1, rootUpdate, cancellationToken: cancellationToken);
        
        return Unit.Value;
    }
}