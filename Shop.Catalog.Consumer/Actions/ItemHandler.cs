using Mapster;
using MediatR;
using MongoDB.Driver;
using Shop.Catalog.Consumer.Dtos;
using Shop.Domain.Requests.Items;

namespace Shop.Catalog.Consumer.Actions;

public class ItemHandler
    : IRequestHandler<CreateItemRequest>,
        IRequestHandler<UpdateItemRequest>,
        IRequestHandler<DeleteItemRequest>
{
    private readonly ILogger<ItemHandler> _logger;
    private readonly IMongoDatabase _database;

    public ItemHandler(IMongoDatabase database, ILogger<ItemHandler> logger)
    {
        _database = database;
        _logger = logger;
    }

    public async Task<Unit> Handle(CreateItemRequest request, CancellationToken cancellationToken)
    {
        var col = _database.GetCollection<CategoryDto>("categories");

        var item = request.Adapt<ItemDto>();
        var parent = request.CategoryId ?? -1;
        var filter = Builders<CategoryDto>.Filter.Eq(x => x.Id, parent);
        var update = Builders<CategoryDto>.Update.Push(x => x.Items, item);
        await col.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);

        return Unit.Value;
    }

    public async Task<Unit> Handle(UpdateItemRequest request, CancellationToken cancellationToken)
    {
        var col = _database.GetCollection<CategoryDto>("categories");

        var category = request.CategoryId ?? -1;
        var item = request.Adapt<ItemDto>();
        var f = Builders<CategoryDto>.Filter.ElemMatch(x => x.Items, x => x.Id == request.Id);
        var filter = Builders<CategoryDto>.Filter.Eq(x => x.Id, category);
        var itemFilter = Builders<ItemDto>.Filter.Eq(x => x.Id, request.Id);
        var update = Builders<CategoryDto>.Update
            .PullFilter(x => x.Items, itemFilter);
        var insert = Builders<CategoryDto>.Update
            .Push(x => x.Items, item);
        await col.UpdateOneAsync(f, update, cancellationToken: cancellationToken);
        await col.UpdateOneAsync(filter, insert, cancellationToken: cancellationToken);
        return Unit.Value;
    }

    public async Task<Unit> Handle(DeleteItemRequest request, CancellationToken cancellationToken)
    {
        var col = _database.GetCollection<CategoryDto>("categories");

        var filter = Builders<CategoryDto>.Filter.ElemMatch(x => x.Items, x => x.Id == request.Id);
        var childFilter = Builders<ItemDto>.Filter.Eq(x => x.Id, request.Id);
        var update = Builders<CategoryDto>.Update.PullFilter(x => x.Items, childFilter);
        await col.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);

        return Unit.Value;
    }
}