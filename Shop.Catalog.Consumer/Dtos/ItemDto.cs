namespace Shop.Catalog.Consumer.Dtos;

public class ItemDto
{
    public int Id { get; set; }
    public string Title { get; set; }
    public int Price { get; set; }
    public string Picture { get; set; }
    public string Description { get; set; } 
}