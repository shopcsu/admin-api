namespace Shop.Catalog.Consumer.Dtos;

public class ChildCategoryDto
{
    public int Id { get; set; }
    public string Title { get; set; }
}