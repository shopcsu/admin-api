using System.Reflection;
using MediatR;
using Shop.Catalog.Consumer;
using Shop.Common.Extensions.Amqp;
using Shop.Common.Extensions.Mongo;

await Host.CreateDefaultBuilder(args)
          .ConfigureServices(services =>
           {
               services.AddMediatR(Assembly.GetExecutingAssembly());
               services.AddMongo();
               services.AddAmqp();
               services.AddHostedService<Worker>();
           })
          .Build()
          .RunAsync();